using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance; //Singleton(design pattern)

    [Header("EVENT:")]
    public static UnityEvent onWin = new UnityEvent(); //tuong tu Observer(design pattern)
    public static UnityEvent onLose = new UnityEvent();

    [Header("ENEMY SET UP:")]
    public GameObject enemyPre;//Doi tuong sinh
    public float timeToSpawn;//Thoi gian sinh doi tuong tiep theo
    private float coolDown = 0;//Dem nguoc thoi gian sinh

    [Space]
    [Header("CONDITION WIN:")]
    public Text txtKill;
    [Tooltip("SL can tieu diet")] public int enemyKill;//SL can tieu diet
    private int enemyCurrentKill;//SL tieu diet hien tai


    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        coolDown -= Time.deltaTime;// Bo dem
        if (coolDown <= 0)//Khi bo dem <= 0 thi spawn
        {
            SpawnEnemy();
            coolDown = timeToSpawn; //Thiet lap lai bo dem
        }
    }

    void SpawnEnemy() //Sinh enemy
    {
        Vector3 spawnPos = new Vector3(Random.Range(-2, 2), 7, 0);
        transform.position = spawnPos;
        Instantiate(enemyPre, spawnPos,Quaternion.identity);
    }

    public void KillEnemy()//Tieu diet ke dich
    {
        enemyCurrentKill++;
        txtKill.text = "Kill: " + enemyCurrentKill;

        if (enemyCurrentKill == enemyKill)
        {
            onWin?.Invoke();
            this.enabled = false;
        }
    }

    public void RePlay()//Reload lai game
    {
        SceneManager.LoadScene(SceneManager.GetSceneAt(0).buildIndex);
    }
}


//Note
//Design Pattern: Observer, Singleton, Object Pooling, State Pattern 