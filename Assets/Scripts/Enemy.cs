using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private void OnEnable()
    {
        GameManager.onWin.AddListener(() => Destroy(gameObject));
    }

    private void OnDisable()
    {
        GameManager.onWin.RemoveListener(() => Destroy(gameObject));        
    }

    public float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= new Vector3(0,moveSpeed,0) * Time.deltaTime;
    }
}
