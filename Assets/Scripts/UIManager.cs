using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Design pattern: Singleton

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public GameObject gameOverUI;
    public GameObject winGameUI;

    private void OnEnable()
    {
        GameManager.onLose.AddListener(() => ShowUI(gameOverUI));//Lang nghe su kien onLose tu thang GameManager phat khi ma doi tuong duoc bat setactive = true
        GameManager.onWin.AddListener(() => ShowUI(winGameUI));//Lang nghe su kien onLose tu thang GameManager phat khi ma doi tuong duoc bat setactive = true
    }

    private void OnDisable()
    {
        GameManager.onLose.RemoveListener(() => ShowUI(gameOverUI));//Huy Lang nghe su kien onLose tu thang GameManager phat khi ma doi tuong duoc bat setactive = false        
        GameManager.onWin.RemoveListener(() => ShowUI(winGameUI));//Lang nghe su kien onLose tu thang GameManager phat khi ma doi tuong duoc bat setactive = true�
    }

    private void Awake()
    {
        instance = this;

        ShowUI(null);
    }

    public void ShowUI(GameObject ui)
    {
        gameOverUI.SetActive(false);
        winGameUI.SetActive(false);

        if (ui)
            ui.SetActive(true);
    }
}
