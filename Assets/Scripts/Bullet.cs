using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            GameManager.instance.KillEnemy();
            collision.gameObject.SetActive(false);//An ke dich khi bi ban trung
            gameObject.SetActive(false);//An vien dan khi ban trung ke dich
        }        
    }
}
