﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Shoot
    public GameObject muzzle; 
    public GameObject bulletPre;
    public Transform shootingPoint;
    public float force;

    //Move
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();

        if (Input.GetMouseButtonDown(1))
        {
            Shoot();
        }
    }

    void Move()
    {
        var dirX = Input.GetAxisRaw("Horizontal");
        var dirY = Input.GetAxisRaw("Vertical");
        transform.position += new Vector3(dirX,dirY,0) * speed * Time.deltaTime;
        transform.position = new Vector3(Mathf.Clamp(transform.position.x,-2,2), Mathf.Clamp(transform.position.y, -4.5f, 4.5f), 0);
    }

    void Shoot()
    {
        GameObject bullet = Instantiate(bulletPre,shootingPoint.position,Quaternion.identity);
        bullet.GetComponent<Rigidbody2D>().AddForce(Vector3.up * force,ForceMode2D.Impulse); //ForceMode: impulse(bỏ g), force(chịu tác động bởi g)
        ShowEffect(muzzle);
    }

    void ShowEffect(GameObject effect)
    {
        effect.SetActive(true);
        effect.GetComponent<Animator>().Rebind();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            gameObject.SetActive(false);
            GameManager.onLose?.Invoke();
        }
    }
}
